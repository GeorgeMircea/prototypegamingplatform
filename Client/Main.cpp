#include"Client.h"

int main()
{
	Client* client = new Client();
	std::cout << "0 - Paper\n1 - Rock\n2 - Scissors\n9 - Exit\n";
	int move=0;
	while (move != 9)
	{
		std::cin >> move;
		switch (move)
		{
		case 0:
			client->sendMessage(Move::PAPER);
			break;
		case 1:
			client->sendMessage(Move::ROCK);
			break;
		case 2:
			client->sendMessage(Move::SCISSORS);
			break;
		default:
			break;
		}
	}
	delete client;
	return 0;
}