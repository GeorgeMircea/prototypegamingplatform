#pragma once
#include<WS2tcpip.h>
#include<iostream>

#include "..\..\Server\Server\NetworkServices.h"
#include "..\..\Server\Server\Packet.h"

#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "6881"
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")


class Client
{
public:
	int m_iResult;
	
	SOCKET m_connectSocket;

	Client();
	~Client();

	void sendMessage(Move);

private:
	void initializeHints(addrinfo & hints);
};

