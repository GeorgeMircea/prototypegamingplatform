#include "Client.h"



Client::Client()
{
	WSADATA wsaData;

	m_connectSocket = INVALID_SOCKET;

	struct addrinfo *result = NULL,
		*ptr = NULL,
		hints;
	m_iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);

	if (m_iResult != 0) {
		std::cout << "WSAStartup failed with error: " << m_iResult << std::endl;
		exit(1);
	}

	ZeroMemory(&hints, sizeof(hints));
	initializeHints(hints);

	m_iResult = getaddrinfo("127.0.0.1", DEFAULT_PORT, &hints, &result);

	if (m_iResult != 0)
	{
		std::cout << "getaddrinfo failed with error: " << m_iResult << std::endl;
		WSACleanup();
		exit(1);
	}

	for (ptr = result; ptr != NULL;ptr = ptr->ai_next)
	{
		m_connectSocket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);

		if (m_connectSocket == INVALID_SOCKET)
		{
			std::cout << "socket failed with error: %ld\n" << WSAGetLastError() << std::endl;
			WSACleanup();
			exit(1);
		}

		m_iResult = connect(m_connectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);

		if (m_iResult == SOCKET_ERROR)
		{
			closesocket(m_connectSocket);
			m_connectSocket = INVALID_SOCKET;
			std::cout << "The server is down... did not connect" << std::endl;
		}
	}

	freeaddrinfo(result);

	if (m_connectSocket == INVALID_SOCKET)
	{
		std::cout << "Unable to connect to server!" << std::endl;
		WSACleanup();
		exit(1);
	}

	u_long iMode = 1;

	m_iResult = ioctlsocket(m_connectSocket, FIONBIO, &iMode);
	if (m_iResult == SOCKET_ERROR)
	{
		std::cout << "ioctlsocket failed with error: " << WSAGetLastError() << std::endl;
		closesocket(m_connectSocket);
		WSACleanup();
		exit(1);
	}
}


Client::~Client()
{
}


void Client::sendMessage(Move move)
{
	const unsigned int packetSize = sizeof(Packet);
	char * packet_data = new char[packetSize];



	Packet packet;
	packet.m_packetType = move;

	packet.serialize(packet_data);
	NetworkServices::sendMessage(m_connectSocket, packet_data, sizeof(packet_data));
}

void Client::initializeHints(addrinfo & hints)
{
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
}
