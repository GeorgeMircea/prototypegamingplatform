#include"Server.h"

Server::Server(void)
{
	WSADATA wsaData;

	m_listenSocket = INVALID_SOCKET;
	m_clientSocket = INVALID_SOCKET;

	struct addrinfo *result = nullptr;
	struct addrinfo hints;

	m_iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);

	if (m_iResult != 0)
	{
		std::cout << "WSAStartup failed with error: " << m_iResult << std::endl;
		exit(1);
	}

	ZeroMemory(&hints, sizeof(hints));
	initializeHints(hints);

	m_iResult = getaddrinfo(nullptr, DEFAULT_PORT, &hints, &result);
	if (m_iResult != 0)
	{
		std::cout << "getaddrinfo failed with error: " << m_iResult << std::endl;
		WSACleanup();
		exit(1);
	}

	m_listenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (m_listenSocket == INVALID_SOCKET)
	{
		std::cout << "socket failed with error: " << WSAGetLastError() << std::endl;
		freeaddrinfo(result);
		WSACleanup();
		exit(1);
	}

	u_long iMode = 1;

	if (ioctlsocket(m_listenSocket, FIONBIO, &iMode) == SOCKET_ERROR) {
		std::cout << "ioctlsocket failed with error: " << WSAGetLastError() << std::endl;
		closesocket(m_listenSocket);
		WSACleanup();
		exit(1);
	}


	if (bind(m_listenSocket, result->ai_addr, (int)result->ai_addrlen) == SOCKET_ERROR)
	{
		std::cout << "bind failed with error: " << WSAGetLastError() << std::endl;
		freeaddrinfo(result);
		closesocket(m_listenSocket);
		WSACleanup();
		exit(1);
	}

	freeaddrinfo(result);

	if (listen(m_listenSocket, SOMAXCONN) == SOCKET_ERROR) {
		std::cout << "listen failed with error: " << WSAGetLastError() << std::endl;
		closesocket(m_listenSocket);
		WSACleanup();
		exit(1);
	}
}

Server::~Server(void)
{
}

bool Server::acceptNewClient(unsigned int & id)
{
	m_clientSocket = accept(m_listenSocket, nullptr, nullptr);

	if (m_clientSocket != INVALID_SOCKET)
	{
		char value = 1;
		setsockopt(m_clientSocket, IPPROTO_TCP, TCP_NODELAY, &value, sizeof(value));

		// insert new client into session id table
		m_sessions.insert(std::pair<unsigned int, SOCKET>(id, m_clientSocket));

		return true;
	}
	return false;
}

int Server::receiveData(unsigned int client_id, char * recvbuf)
{
	if (m_sessions.find(client_id) != m_sessions.end())
	{
		SOCKET currentSocket = m_sessions[client_id];
		m_iResult = NetworkServices::receiveMessage(currentSocket, recvbuf, MAX_PACKET_SIZE);
		if (m_iResult == 0)
		{
			printf("Connection closed\n");
			closesocket(currentSocket);
		}
		return m_iResult;
	}
	return 0;
}

void Server::initializeHints(addrinfo & hints)
{
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;
}
