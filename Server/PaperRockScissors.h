#pragma once

enum class Move
{
	PAPER=0,
	ROCK=1,
	SCISSORS=2
};
class PaperRockScissors
{
	Move m_move;
public:
	PaperRockScissors();
	~PaperRockScissors();

	//returns -1 if player 1 wins, 0 if it's a draw and 1 if player 2 wins
	int getWinner(Move) const;

	void setMove(Move a_move);
};

