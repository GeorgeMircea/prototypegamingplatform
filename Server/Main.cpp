#include"GameServer.h"
#pragma comment(lib, "Ws2_32.lib")

void serverLoop(GameServer* game)
{
	while (true)
	{
		game->update();
	}
}

int main()
{
	GameServer * game = new GameServer();
	
	serverLoop(game);

	delete game;

	return 0;
}