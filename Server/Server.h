#pragma once
#include<WinSock2.h>
#include<Windows.h>
#include<WS2tcpip.h>
#include<map>
#include<iostream>

#include"NetworkServices.h"
#include"Packet.h"

#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "6881" 

class Server
{
public:
	Server(void);
	~Server(void);

	bool acceptNewClient(unsigned int & id);
	int receiveData(unsigned int client_id, char * recvbuf);

	SOCKET m_listenSocket;
	SOCKET m_clientSocket;


	std::map<unsigned int, SOCKET> m_sessions;

private:
	int m_iResult;
	void initializeHints(addrinfo & hints);
};