#pragma once
#include"Server.h"

class GameServer
{
	bool m_newTurn=true;
	PaperRockScissors m_game;
public:
	GameServer();
	~GameServer();

	void update();
	void receiveFromClient();

private:
	static unsigned int client_id;
	Server* m_server;
	char * m_networkData=new char[MAX_PACKET_SIZE];

};

