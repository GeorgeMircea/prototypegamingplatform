#pragma once
#include <string>
#include"PaperRockScissors.h"

#define MAX_PACKET_SIZE 1000000

class Packet
{
public:
	Packet();
	~Packet();

	//std::string m_data;

	Move m_packetType;

	void serialize(char * data);
	void deserialize(char * data);
};

