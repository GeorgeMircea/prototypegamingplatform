#include "GameServer.h"

unsigned int GameServer::client_id;

GameServer::GameServer()
{
	client_id = 0;
	m_server = new Server();
}


GameServer::~GameServer()
{
}

void GameServer::update()
{
	if (m_server->acceptNewClient(client_id))
	{
		std::cout<<"client " << client_id << " has been connected to the server" << std::endl;
		++client_id;
	}
	receiveFromClient();
}

void GameServer::receiveFromClient()
{
	Packet packet;

	// go through all clients
	std::map<unsigned int, SOCKET>::iterator iter;

	for (iter = m_server->m_sessions.begin(); iter != m_server->m_sessions.end(); iter++)
	{
		// get data for that client
		int data_length = m_server->receiveData(iter->first, m_networkData);

		if (data_length <= 0)
		{
			//no data recieved
			continue;
		}

		int i = 0;
		while (i < (unsigned int)data_length)
		{
			packet.deserialize(&(m_networkData[i]));
			i += sizeof(packet.m_packetType);
			
			if (m_newTurn)
				m_game.setMove(packet.m_packetType);
			else
			{
				int winner = m_game.getWinner(packet.m_packetType);
				switch (winner)
				{
				case -1:
					std::cout << "Player 1 wins!" << std::endl;
					break;
				case 0:
					std::cout << "Draw!" << std::endl;
					break;
				case 1:
					std::cout << "Player 2 wins!" << std::endl;
					break;
				default:
					break;
				}
			}

			m_newTurn = !m_newTurn;
		}
	}
}
