#include "PaperRockScissors.h"



PaperRockScissors::PaperRockScissors()
{
}


PaperRockScissors::~PaperRockScissors()
{
}

int PaperRockScissors::getWinner(Move a_move) const
{
	if (m_move == a_move)
		return 0;

	if ((int)m_move - (int)a_move == 1)
		return 1;

	return -1;
}

void PaperRockScissors::setMove(Move a_move)
{
	m_move = a_move;
}
